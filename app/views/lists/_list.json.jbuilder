json.extract! list, :id, :uid, :customer_email, :name, :subject, :body, :recipient_email_address, :date_sent, :select, :created_at, :updated_at
json.url list_url(list, format: :json)
