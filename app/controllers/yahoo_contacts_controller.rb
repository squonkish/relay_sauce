class YahooContactsController < ApplicationController
  def new
    @auth = request.env['omniauth.auth']['credentials']

    yahoo_gateway = YahooGateway.new(*@auth.values_at('token', 'expires_at', 'refresh_token'))
    @emails       = yahoo_gateway.fetch_contact_emails

    LoadOrCreateEmailStash.new(current_user, @emails, "yahoo").call

    redirect_to email_stashes_index_yahoo_url
    @u_email_stash = current_user.user_email_stashes.includes(:email_stash)

  end

end
