class EmailStashesController < ApplicationController
  before_action :authenticate_user!
  before_action :set_email_stash, only: [:show, :edit, :update, :destroy]

  # GET /email_stashs
  # GET /email_stashs.json
  def index_gmail
    @u_email_stash = current_user.user_email_stashes.includes(:email_stash)
    @email_stash = EmailStash.new(source: "gmail")
    params[:selected] = true
  end

  def index_yahoo
    @u_email_stash = current_user.user_email_stashes.includes(:email_stash)
    params[:selected] = true
    @email_stash = EmailStash.new(source: "yahoo")
  end

  def create_recipient
    email_stash = EmailStash.where(recipient_email_address: new_email_stash_params[:recipient_email_address]).first
    source = new_email_stash_params[:source]
    if email_stash
      current_user.user_email_stashes.find_or_create_by email_stash_id: email_stash.id
      redirect_to(source == "gmail" ? email_stashes_index_gmail_path : email_stashes_index_yahoo_path)
    else
      @email_stash = EmailStash.new new_email_stash_params
      if @email_stash.save
        UserEmailStash.create user_id: current_user.id, email_stash_id: @email_stash.id
        redirect_to(source == "gmail" ? email_stashes_index_gmail_path : email_stashes_index_yahoo_path)
      else
        @u_email_stash = current_user.user_email_stashes.includes(:email_stash)
        render(source == "gmail" ? :index_gmail : :index_yahoo)
      end
    end
  end

  # GET /email_stashs/1
  # GET /email_stashs/1.json
  def show
    @email_stash = EmailStash.find(params[:id])
  end

  # GET /email_stashs/1/edit
  def edit
    
  end

  def selection
    # byebug
    @u_email_stash = UserEmailStash.where id: params[:u_email_stash].split(',')
    respond_to do |format|
      @u_email_stash.each do |e|
        if e.id.to_s.in?(params[:u_email_stash_ids].presence || [])
          e.update! selected: true
          redirect_path = params[:source] == "gmail" ? email_stashes_index_gmail_path : email_stashes_index_yahoo_path
          format.html { redirect_to redirect_path, notice: 'Email list was successfully updated.' }
        else
          e.update! selected: false
        end
      end
    end
    # respond_to do |format|
    #     if @email_stash.update
    #       format.html { redirect_to new_email_template_path, notice: 'Email list was successfully updated.' }
    #       # flash[:toastr] = "Updated emails!"
    #       format.json { render :show, status: :ok, location: new_email_template_path }
    #     end
    #   end
    
    
  end

  # POST /email_stashs
  # POST /email_stashs.json
  def create
    @email_stash = EmailStash.new(email_stash_params)

    respond_to do |format|
      if @email_stash.save
        format.html { redirect_to @email_stash, notice: 'Email stash was successfully created.' }
        format.json { render :show, status: :created, location: @email_stash }
      else
        format.html { render :new }
        format.json { render json: @email_stash.errors, status: :unprocessable_entity }
      end
    end
  end

  # GET /email_stashs/new
  def new
    @auth = request.env['omniauth.auth']['credentials']

    yahoo_gateway = YahooGateway.new(*@auth.values_at('token', 'expires_at', 'refresh_token'))
    @emails       = yahoo_gateway.fetch_contact_emails


    #make array of recipients
    # @stash = EmailStash.new(uid: current_user.uid, email: current_user.email, name: current_user.name, selected: true, recipient_email_addresses: @recipients)
    # @recipients = []
    # @emails.each do |d|
    #   @recipients << d
    # end
    # @stash.save

    # @count = EmailStash.where(uid: current_user.uid).count
    # @newlist = List.new(title: 'Facebook' + @count)

    # respond_to do |format|
    #   if @newlist.save
    #     format.html { redirect_to @list, notice: 'List was successfully created.' }
    #   end
    # end

    @email_stash = EmailStash.all

  end


  # PATCH/PUT /email_stashs/1
  # PATCH/PUT /email_stashs/1.json
  def update
    respond_to do |format|
      if @email_stash.update(email_stash_params)
        format.html { redirect_to @email_stash, notice: 'Email stash was successfully updated.' }
        # flash[:toastr] = "Updated emails!"
        format.json { render :show, status: :ok, location: @email_stash }
      else
        format.html { render :edit }
        format.json { render json: @email_stash.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /email_stashs/1
  # DELETE /email_stashs/1.json
  def destroy
    @email_stash.destroy
    respond_to do |format|
      format.html { redirect_to email_stashs_url, notice: 'Email stash was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_email_stash
      @email_stash = EmailStash.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white stash through.
    def email_stash_params
      params.require(:email_stash).permit(:uid, :recipient_name, :email, :date_sent, :subject, :body, :recipient_email_address)
    end

    def new_email_stash_params
      params.require(:email_stash).permit(:email, :recipient_email_address, :source,
        :recipient_first_name, :recipient_last_name)
    end
end
