class UnsubscribesController < ApplicationController
  def show
    @mailer = UserMailerStash.where(unsubscribe_id: params[:id]).first
    if @mailer
      email_stash = @mailer.email_stash
      email_stash.update_attributes is_receive_mail: false
    end
  end
end
