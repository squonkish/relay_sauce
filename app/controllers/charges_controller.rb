class ChargesController < ApplicationController
  before_action :authenticate_user!

  def new
    @email_stash = EmailStash.count
    @amount = current_user.user_email_stashes.selected.count * 6
    @amount_sum = current_user.user_email_stashes.selected.count * 0.06
    @email_template = EmailTemplate.where(uid: current_user.uid, id: params[:template_id]).first
    return redirect_to(email_templates_path) unless @email_template
    # @amount.number_to_currency
    @count = current_user.user_email_stashes.selected.count
  end

  def create
    # Amount in cents
    @email_template = EmailTemplate.where(uid: current_user.uid, id: params[:template_id]).first
    if @email_template.nil?
      return redirect_to(new_charge_path(template_id: params[:template_id]))
    end
    @amount = current_user.user_email_stashes.selected.count * 6

    # @amount.number_to_currency
    if @amount > 1500
      customer = Stripe::Customer.create(
        :email => params[:stripeEmail],
        :source  => params[:stripeToken]
      )

      charge = Stripe::Charge.create(
        :customer    => customer.id,
        :amount      => @amount,
        :description => 'Rails Stripe customer',
        :currency    => 'usd'
      )
    else
      customer = Stripe::Customer.create(
        :email => params[:stripeEmail],
        :source  => params[:stripeToken]
      )

      charge = Stripe::Charge.create(
        :customer    => customer.id,
        :amount      => '1500',
        :description => 'Rails Stripe customer',
        :currency    => 'usd'
      )
    end
    create_mailer

  rescue Stripe::CardError => e
    flash[:error] = e.message
    redirect_to new_charge_path
  end

  private
  def create_mailer
    @email_stashes = current_user.email_stashes.received_mail
    ActiveRecord::Base.transaction do
      mailer = UserMailer.create! user_id: current_user.id,
        email_template_id: @email_template.id, subject: @email_template.subject,
        body: @email_template.body
      @email_stashes.each do |email_stash|
        UserMailerStash.create! user_mailer_id: mailer.id,
          email_stash_id: email_stash.id
      end
    end
  end
end
