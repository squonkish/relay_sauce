class GoogleContactsController < ApplicationController
  def new
    @auth = request.env['omniauth.auth']['credentials']

    google_gateway = GoogleGateway.new(@auth['token'], @auth['expires_at'])
    @emails = google_gateway.fetch_contact_emails

    LoadOrCreateEmailStash.new(current_user, @emails, "gmail").call

    redirect_to email_stashes_index_gmail_url
    @u_email_stash = current_user.user_email_stashes.includes(:email_stash)

  end
end