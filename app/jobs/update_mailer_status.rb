class UpdateMailerStatus < ActiveJob::Base
  queue_as :default

  def perform
    SqsService.new.execute
  end
end
