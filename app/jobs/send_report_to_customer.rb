class SendReportToCustomer < ActiveJob::Base
  queue_as :default

  def perform user_mailer_id, final_report = false
    user_mailer = UserMailer.find user_mailer_id
    SesMailer.new.report_rate_mail user_mailer, user_mailer.user, final_report
  end
end
