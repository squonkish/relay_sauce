#= require redactor/redactor
#= require redactor/alignment
#= require redactor/counter
#= require redactor/fontcolor
#= require redactor/fullscreen
#= require redactor/inlinestyle
#= require redactor/properties
#= require redactor/table
#= require redactor/video
#= require redactor/widget

$ ->
  $R '.js-redactor', {
    plugins: ['counter', 'fontcolor', 'fullscreen', 'inlinestyle', 'table']
  }
