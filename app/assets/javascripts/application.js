// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require bootstrap-sprockets
//= require select_all.js
//= require_tree .
//= require toastr
//= require text_editor

$(window).bind('beforeunload', function() {
  $('.loading').css({'display':'block'});
});

$(window).on("load",function(){
  $(".loading").fadeOut("slow");
});

$(document).ready(function() {
  $(".loading").fadeOut("slow");
  $('.tooltip').tooltip();
	
	toastr.options = {
	                  "closeButton": false,
	                  "debug": false,
	                  "positionClass": "toast-bottom-right",
	                  "onclick": null,
	                  "showDuration": "300",
	                  "hideDuration": "1000",
	                  "timeOut": "5000",
	                  "extendedTimeOut": "1000",
	                  "showEasing": "swing",
	                  "hideEasing": "linear",
	                  "showMethod": "fadeIn",
	                  "hideMethod": "fadeOut"
	              }
	//progress bar 
	$(".lds-rolling").hide();

	  $(document).ajaxStart(function() {
      $(".loading").fadeOut("slow");
	  }).ajaxStop(function() {
	      $(".lds-rolling").hide();
	  });

	});
