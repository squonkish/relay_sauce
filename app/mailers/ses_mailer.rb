class SesMailer
  DEFAULT_FROM = "support@relaysauce.com"
  NO_REPLY_FROM = "no-reply@relaysauce.com"

  CHARSET = "UTF-8"

  def campaign_email user, user_mailer, email_stash, unsubscribe_id
    message = message_content user, user_mailer, email_stash,
      "campaign_email", unsubscribe_id
    ses.send_email message
  end

  def test_email email_template, recipient
    message = test_message_content email_template, recipient, "test_email"
    ses.send_email message
  end

  def confirm_mail user_mailer, customer
    message = confirm_mail_content user_mailer, customer
    ses.send_email message
  end

  def report_rate_mail user_mailer, customer, final_report = false
    message = report_rate_mail_content user_mailer, customer, final_report
    ses.send_email message
  end

  private
  def mail_body locals, partial
    ApplicationController.renderer
      .new(http_host: ENV['HOST_NAME']).render(
      partial: "ses_mailer/#{partial}",
      locals: locals,
      layout: false
    )
  end

  def message_content user, user_mailer, email_stash, partial, unsubscribe_id
    locals = {name: email_stash.send_mail_name, body: user_mailer.body,
      unsubscribe_id: unsubscribe_id}
    body = mail_body locals, partial
    message_body user_mailer.subject, body, email_stash.recipient_email_address, user.name,
      user.email
  end

  def test_message_content email_template, recipient, partial
    locals = {name: email_template.name.split(" ").first, body: email_template.body}
    body = mail_body locals, partial
    message_body email_template.subject, body, recipient, email_template.name,
      email_template.email
  end

  def confirm_mail_content user_mailer, customer
    locals = {name: customer.name, subject: user_mailer.subject, body: user_mailer.body,
      total_user: user_mailer.user_mailer_stashes.count, ordered_at: user_mailer.created_at.strftime("%Y-%m-%d %H:%m")}
    body = mail_body locals, "confirm_mail"
    custom_message_body customer.email, customer.name, "Your order is confirmed and complete!", body
  end

  def report_rate_mail_content user_mailer, customer, final_report = false
    locals = {name: customer.name, total_user: user_mailer.user_mailer_stashes.count,
      ordered_at: user_mailer.created_at.strftime("%Y-%m-%d"), final_report: final_report,
      open_rate: user_mailer.user_mailer_stashes.open_mail.count}
    body = mail_body locals, "report_rate_mail"
    custom_message_body customer.email, customer.name, "[Relaysauce] Report for open rate mail!", body
  end

  def message_body subject, body, email_to, name, reply_email
    {
      destination: {
        to_addresses: [email_to],
      },
      message: {
        body: {
          html: {
            charset: CHARSET,
            data: body
          }
        },
        subject: {
          charset: CHARSET,
          data: subject
        }
      },
      reply_to_addresses: [reply_email],
      source: "#{name} <#{DEFAULT_FROM}>",
      configuration_set_name: ENV["SES_CONFIGURATION_SET_NAME"]
    }
  end

  def custom_message_body email_to, name, subject, body
    {
      destination: {
        to_addresses: [email_to],
      },
      message: {
        body: {
          html: {
            charset: CHARSET,
            data: body
          }
        },
        subject: {
          charset: CHARSET,
          data: subject
        }
      },
      source: "Relaysauce <#{NO_REPLY_FROM}>",
      configuration_set_name: ENV["SES_CONFIGURATION_SET_NAME"]
    }
  end

  def ses
    @ses ||= Aws::SES::Client.new ses_options
  end

  def ses_options
    {
      access_key_id: ENV["AWS_ACCESS_KEY"],
      secret_access_key: ENV["AWS_SECRET_ACCESS_KEY"],
      region: "us-west-2",
      raise_response_errors: false
    }
  end

end
