class ApplicationMailer < ActionMailer::Base

	layout nil

    def customer_email(email_template, recipient)
      # sendgrid_category :use_subject_lines
      # sendgrid_unique_args :key2 => "newvalue2", :key3 => "value3"
      @receiver_first_name = email_template.name
      @body = email_template.body.to_s.html_safe
      # @e = EmailStash.where(uid: email_template.uid, source: 'yahoo').map(&:recipient_email_address)

      mail :to => recipient, :subject => email_template.subject, :reply_to => email_template.email, :from => 'relaysauce@gmail.com'
    end

  end
