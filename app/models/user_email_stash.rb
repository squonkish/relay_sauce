class UserEmailStash < ApplicationRecord
  validates_uniqueness_of :email_stash_id, scope: :user_id

  belongs_to :user
  belongs_to :email_stash

  scope :selected, -> {where(selected: true)}
end
