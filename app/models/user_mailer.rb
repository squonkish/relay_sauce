class UserMailer < ApplicationRecord
  belongs_to :user
  belongs_to :email_template

  has_many :user_mailer_stashes
end
