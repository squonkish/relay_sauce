class UserMailerStash < ApplicationRecord
  belongs_to :user_mailer
  belongs_to :email_stash

  scope :open_mail, -> {where(is_open: true)}
end
