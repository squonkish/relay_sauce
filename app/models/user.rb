class User < ActiveRecord::Base

  devise :omniauthable, omniauth_providers: [:facebook]

  has_many :user_mailers
  has_many :user_email_stashes
  has_many :selected_user_email_stashes, -> {selected}, class_name: UserEmailStash.name
  has_many :email_stashes, through: :selected_user_email_stashes

  def self.create_from_omniauth(params)
  	user = find_or_create_by(email: params.info.email, uid: params.uid)
    user.update({
  		token: params.credentials.token,
			name: params.info.name,
			avatar: params.info.image
    })
    user
  end
end
