class EmailStash < ApplicationRecord
  # serialize :recipient_email_addresses, Array
  validates :recipient_email_address, presence: true
  validates_format_of :recipient_email_address, with: /\A[\w+\-.]+@[a-z\d\-]+(\.[a-z\d\-]+)*\.[a-z]+\z/, on: :create
  validates :recipient_email_address, uniqueness: true

  scope :received_mail, -> {where(is_receive_mail: true)}
  def send_mail_name
    if recipient_first_name.present?
      recipient_first_name
    end
  end

  def recipient_full_name
    [recipient_first_name, recipient_last_name].join " "
  end
end
