class SqsService
  MAX_POLL_MESSAGES = 100
  EMAIL_STATUSES = %i(Delivery Complaint Open)

  attr_reader :poller

  def initialize
    queue_url = ENV['EMAIL_QUEUE']
    @poller = Aws::SQS::QueuePoller.new queue_url, poller_options
    stop_queue_polling
  end

  def execute
    mailer_statuses = {
      Delivery: [],
      Complaint: [],
      Open: []
    }
    poller.poll do |message|
      begin
        message_body = queue_body_parse message.try(:body)
        notification_message = queue_body_parse message_body[:Message]
        email_status = notification_message[:notificationType] ||
          notification_message[:eventType]
        next unless email_status.to_sym.in?(EMAIL_STATUSES)
        mailer_statuses[email_status.to_sym] << notification_message[:mail]["messageId"]
      rescue StandardError
        throw :skip_delete
      end
    end

    update_mailer_status mailer_statuses
  end

  private
  def stop_queue_polling
    poller.before_request do |stats|
      throw :stop_polling if stats.received_message_count >= MAX_POLL_MESSAGES
    end
  end

  def queue_body_parse queue_body
    queue_body_obj = JSON.parse queue_body
    queue_body_obj.symbolize_keys
  rescue JSON::ParserError => json_parser_error
    raise "Error parse queue message body: #{json_parser_error}"
  end

  def update_mailer_status mailer_statuses
    mailer_statuses.each do |status, message_ids|
      case status
      when EMAIL_STATUSES[0], EMAIL_STATUSES[1]
        update_status message_ids, status
      when EMAIL_STATUSES[2]
        update_open_status message_ids
      end
    end
  end

  def update_status message_ids, status
    UserMailerStash.where(message_id: message_ids).update_all email_status: status
  end

  def update_open_status message_ids
    UserMailerStash.where(message_id: message_ids).update_all is_open: true
  end

  def poller_options
    {
      client: client,
      wait_time_seconds: 20,
      skip_delete: false,
      visibility_timeout: 10,
      idle_timeout: 10
    }
  end

  def client
    @client ||= Aws::SQS::Client.new(
      access_key_id: ENV["AWS_ACCESS_KEY"],
      secret_access_key: ENV["AWS_SECRET_ACCESS_KEY"],
      region: "us-west-2",
      raise_response_errors: false
    )
  end
end
