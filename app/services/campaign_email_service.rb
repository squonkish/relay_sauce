class CampaignEmailService

  attr_reader :user_mailer, :user

  def initialize user_mailer_id
    @user_mailer = UserMailer.find user_mailer_id
    @user = user_mailer.user
  end

  def send_mail
    user_mailer.start_at = Time.current
    user_mailer.user_mailer_stashes.includes(:email_stash).each do |u_mailer_stash|
      next unless u_mailer_stash.email_stash.is_receive_mail
      unsubscribe_id = SecureRandom.hex
      mailer = SesMailer.new.campaign_email(user, user_mailer,
        u_mailer_stash.email_stash, unsubscribe_id)
      u_mailer_stash.update_attributes message_id: mailer.try(:message_id),
        unsubscribe_id: unsubscribe_id
    end
    user_mailer.end_at = Time.current
    user_mailer.save
    send_confirm_mail user_mailer
    SendReportToCustomer.set(wait: 48.hours).perform_later(user_mailer.id)
    SendReportToCustomer.set(wait: 1.week).perform_later(user_mailer.id, true)
  end

  def send_confirm_mail user_mailer
    SesMailer.new.confirm_mail user_mailer, user_mailer.user
  end
end
