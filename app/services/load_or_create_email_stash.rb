class LoadOrCreateEmailStash

  attr_reader :user, :emails, :source

  def initialize user, emails, source
    @emails = emails
    @source = source
    @user = user
  end

  def call
    emails.each do |e|
      if stash = EmailStash.where(recipient_email_address: e[:email]).first
        stash.recipient_first_name = e[:first_name] if e[:first_name].present?
        stash.recipient_last_name = e[:last_name] if e[:last_name].present?
        stash.save
      else
        stash = EmailStash.create recipient_email_address: e[:email], uid: user.uid,
          email: user.email, recipient_first_name: e[:first_name],
          recipient_last_name: e[:last_name], source: source
      end
      user.user_email_stashes.where(email_stash_id: stash.id).first_or_create
    end
  end
end
