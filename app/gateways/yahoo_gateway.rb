class YahooGateway
  CONTACTS_ENDPONT  = 'https://social.yahooapis.com/v1/user/me/contacts?format=json'
  TIMEOUT           = 30
  OAUTH_CLIENT      = OAuth2::Client.new(ENV['YAHOO_CLIENT_ID'], ENV['YAHOO_CLIENT_SECRET'])

  def initialize(access_token, expires_at, refresh_token)
    @oauth2_access_token = OAuth2::AccessToken.new(OAUTH_CLIENT, access_token,
      refresh_token:  refresh_token,
      expires_at:     expires_at
    )
  end

  def fetch_contact_emails
    uri = URI(CONTACTS_ENDPONT)

    request = Net::HTTP::Get.new(uri.request_uri)
    request['authorization'] = "Bearer #{@oauth2_access_token.token}"

    timeout(TIMEOUT) do
      response = net_http(uri).request(request)

      contacts_json = JSON.parse(response.body)['contacts']['contact']
      contacts_json.map do |contact|
        email_field = contact["fields"].find{|f| f["type"] == "email"}
        name_field = contact["fields"].find{|f| f["type"] == "name"}
        email = email_field["value"] if email_field
        first_name = name_field["value"]["givenName"] if name_field
        last_name = name_field["value"]["familyName"] if name_field

        {
          email: email,
          first_name: first_name,
          last_name: last_name
        }
      end
    end
  end


  protected

  def net_http(uri)
    http = Net::HTTP.new(uri.host, uri.port)
    http.use_ssl = true
    http.verify_mode = OpenSSL::SSL::VERIFY_NONE
    http
  end
end
