class GoogleGateway
  OAUTH_CLIENT = OAuth2::Client.new(ENV['GOOGLE_CLIENT_ID'], ENV['GOOGLE_CLIENT_SECRET'])

  def initialize(access_token, expires_at)
    @oauth2_access_token = OAuth2::AccessToken.new(OAUTH_CLIENT, access_token,
      expires_at: expires_at
    )
  end

  def fetch_contact_emails
    google_contacts_user = GoogleContactsApi::User.new(@oauth2_access_token)
    google_contacts_user.contacts.map do |contact|
      next unless contact.primary_email
      {
        email: contact.primary_email,
        first_name: contact.given_name,
        last_name: contact.family_name
      }
    end.compact
  end
end
