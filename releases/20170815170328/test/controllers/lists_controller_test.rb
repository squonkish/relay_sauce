require 'test_helper'

class ListsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @list = lists(:one)
  end

  test "should get index" do
    get lists_url
    assert_response :success
  end

  test "should get new" do
    get new_list_url
    assert_response :success
  end

  test "should create list" do
    assert_difference('List.count') do
      post lists_url, params: { list: { body: @list.body, customer_email: @list.customer_email, date_sent: @list.date_sent, name: @list.name, recipient_email_address: @list.recipient_email_address, select: @list.select, subject: @list.subject, uid: @list.uid } }
    end

    assert_redirected_to list_url(List.last)
  end

  test "should show list" do
    get list_url(@list)
    assert_response :success
  end

  test "should get edit" do
    get edit_list_url(@list)
    assert_response :success
  end

  test "should update list" do
    patch list_url(@list), params: { list: { body: @list.body, customer_email: @list.customer_email, date_sent: @list.date_sent, name: @list.name, recipient_email_address: @list.recipient_email_address, select: @list.select, subject: @list.subject, uid: @list.uid } }
    assert_redirected_to list_url(@list)
  end

  test "should destroy list" do
    assert_difference('List.count', -1) do
      delete list_url(@list)
    end

    assert_redirected_to lists_url
  end
end
