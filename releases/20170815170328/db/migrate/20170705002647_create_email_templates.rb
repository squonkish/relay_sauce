class CreateEmailTemplates < ActiveRecord::Migration[5.0]
  def change
    create_table :email_templates do |t|
      t.text :body
      t.string :subject
      t.string :email
      t.string :uid
      t.string :url
      t.string :name
      t.text :recipients_list

      t.timestamps
    end
  end
end
