class CreateLists < ActiveRecord::Migration[5.0]
  def change
    create_table :lists do |t|
      t.string :uid
      t.string :email
      t.string :name
      t.string :title
      t.string :subject
      t.text :body
      t.text :recipients
      t.date :date_sent, null: true
      t.boolean :selected

      t.timestamps
    end
  end
end
