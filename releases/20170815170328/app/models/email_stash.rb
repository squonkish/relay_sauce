class EmailStash < ApplicationRecord
	# serialize :recipient_email_addresses, Array
	validates :recipient_email_address, uniqueness: true
end
