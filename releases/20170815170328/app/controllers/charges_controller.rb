class ChargesController < ApplicationController
	before_action :authenticate_user!
	
	def new
		@email_stash = EmailStash.count
		@amount = EmailStash.where(uid: current_user.uid).count * 6 
		# @amount.number_to_currency
	end

	def create
	  # Amount in cents

	  @amount = EmailStash.where(uid: current_user.uid).count * 6 
	  # @amount.number_to_currency
	  if @amount > 3000
	  	customer = Stripe::Customer.create(
	  	  :email => params[:stripeEmail],
	  	  :source  => params[:stripeToken]
	  	)

	  	charge = Stripe::Charge.create(
	  	  :customer    => customer.id,
	  	  :amount      => @amount,
	  	  :description => 'Rails Stripe customer',
	  	  :currency    => 'usd'
	  	)
	  else
	  	customer = Stripe::Customer.create(
	  	  :email => params[:stripeEmail],
	  	  :source  => params[:stripeToken]
	  	)

	  	charge = Stripe::Charge.create(
	  	  :customer    => customer.id,
	  	  :amount      => '3000',
	  	  :description => 'Rails Stripe customer',
	  	  :currency    => 'usd'
	  	)
	  end

	  

	rescue Stripe::CardError => e
	  flash[:error] = e.message
	  redirect_to new_charge_path
	end
end
