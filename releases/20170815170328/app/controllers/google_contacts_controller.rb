class GoogleContactsController < ApplicationController
  def new
    @auth = request.env['omniauth.auth']['credentials']

    google_gateway = GoogleGateway.new(@auth['token'], @auth['expires_at'])
    @emails = google_gateway.fetch_contact_emails

    @emails.each do |e|
      @stash = EmailStash.new(recipient_email_address: e)
      @stash.uid = current_user.uid
      @stash.email = current_user.email
      @stash.recipient_first_name = current_user.name
      @stash.source = 'gmail'
      @stash.save
      # 
    end
    redirect_to email_stashes_index_gmail_url
    @email_stash = EmailStash.where(uid: current_user.uid, source: 'gmail')

  end
end