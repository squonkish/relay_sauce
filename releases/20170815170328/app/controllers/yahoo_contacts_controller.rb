class YahooContactsController < ApplicationController
  def new
    @auth = request.env['omniauth.auth']['credentials']

    yahoo_gateway = YahooGateway.new(*@auth.values_at('token', 'expires_at', 'refresh_token'))
    @emails       = yahoo_gateway.fetch_contact_emails

    @emails.each_with_index do |e, i|
      @stash = EmailStash.new(recipient_email_address: e["email"])
      @stash.uid = current_user.uid
      @stash.email = current_user.email
      @stash.recipient_first_name = e["first_name"]
      @stash.recipient_last_name = e["last_name"]
      @stash.source = 'yahoo'
      @stash.save
      
    end
    redirect_to email_stashes_index_yahoo_url
    @email_stash = EmailStash.where(uid: current_user.uid)

  end

end
