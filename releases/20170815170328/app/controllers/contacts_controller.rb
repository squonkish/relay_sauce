class ContactsController < ApplicationController
  def new
    @contact = Contact.new
  end

  def create
    @contact = Contact.new(params[:contact])
    @contact.request = request
    
    
    respond_to do |format|
      if @contact.save
        format.html { redirect_to '/', notice: 'Your message has been sent. You will hear from us soon' }
        format.json { render :show, status: :created, location: @contact }
        # ApplicationMailer.customer_email(@email_template, @recipients).deliver
      else
        format.html { render :new }
        format.json { render json: @contact.errors, status: :unprocessable_entity }
      end
    end
  end
end