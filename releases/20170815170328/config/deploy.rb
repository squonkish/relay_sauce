# config valid only for current version of Capistrano
lock '3.9.0'

# core
set :user,         'root'
set :application,  'relay-sauce'
set :repo_url,     'git@bitbucket.org:squonkish/relay-sauce.git'
set :deploy_to,    "/home/rails/#{fetch(:application)}"
set :log_level,    :info
set :linked_files, fetch(:linked_files, []).push('config/database.yml')
set :linked_dirs,  fetch(:linked_dirs, []).push('log', 'tmp/pids', 'tmp/cache', 'tmp/sockets', 'vendor/bundle', 'public/system')
# set :ssh_options, { forward_agent: true, auth_methods: %w(publickey) }
# set :default_env, { path: "/opt/ruby/bin:$PATH" }
# set :keep_releases, 5

# rvm
#set :rvm_ruby_version, '2.2.4'

# rails
set :rails_env,  :production
set :keep_assets, 2

# NOTE: Using the below foreman namespace requires that the user is given access
# to passwordless sudo for the commands: `foreman`, `start`, `stop`, and
# `restart`. You can add the following line to `sudo visudo`:
#
# relay ALL=NOPASSWD: /home/relay/.rbenv/shims/foreman, /sbin/start, /sbin/stop, /sbin/restart
#
namespace :foreman do
  desc "Export application processes as Upstart jobs"
  task :export do
    on roles(:app) do
      within current_path do
        with rails_env: fetch(:rails_env) do
          execute "cd #{fetch :release_path}; /usr/local/rvm/bin/rvm 2.4.0 do bundle exec foreman export upstart /etc/init --app #{fetch :application} --user #{fetch :user} --log #{fetch :foreman_log}"
        end
      end
    end
  end

  desc "Start Application"
  task :start do
    on roles(:app) do
      execute :start, fetch(:application)
    end
  end

  desc "Stop Application"
  task :stop do
    on roles(:app) do
      execute :stop, fetch(:application)
    end
  end

  desc 'Restart Application'
  task :restart do
    on roles(:app) do
      execute :restart, fetch(:application), raise_on_non_zero_exit: false
      execute :start, fetch(:application),   raise_on_non_zero_exit: false
    end
  end
end

#before 'deploy:migrate', 'linked_files:upload_files'
#before 'deploy:assets:precompile', 'linked_files:upload_files'

namespace :deploy do
  desc 'Restart application'
  task :restart do
    on roles(:app), in: :sequence, wait: 10 do
      invoke 'puma:restart'
    end
  end

  #after :publishing, "foreman:export"
  #after :publishing, "foreman:restart"
  # after  :finishing,    :restart
  # after "deploy:finished", 'airbrake:deploy'
end
