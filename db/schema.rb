# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170908155803) do

  create_table "admins", force: :cascade do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.index ["email"], name: "index_admins_on_email", unique: true
    t.index ["reset_password_token"], name: "index_admins_on_reset_password_token", unique: true
  end

  create_table "email_stashes", force: :cascade do |t|
    t.string   "recipient_email_address"
    t.string   "uid"
    t.string   "recipient_first_name"
    t.string   "recipient_last_name"
    t.string   "email"
    t.date     "date_sent"
    t.boolean  "selected",                default: true
    t.string   "source"
    t.datetime "created_at",                             null: false
    t.datetime "updated_at",                             null: false
    t.boolean  "is_receive_mail",         default: true
    t.index ["is_receive_mail"], name: "index_email_stashes_on_is_receive_mail"
  end

  create_table "email_templates", force: :cascade do |t|
    t.text     "body"
    t.string   "subject"
    t.string   "email"
    t.string   "uid"
    t.string   "url"
    t.string   "name"
    t.text     "recipients_list"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
  end

  create_table "lists", force: :cascade do |t|
    t.string   "uid"
    t.string   "email"
    t.string   "name"
    t.string   "title"
    t.string   "subject"
    t.text     "body"
    t.text     "recipients"
    t.date     "date_sent"
    t.boolean  "selected"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "user_email_stashes", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "email_stash_id"
    t.boolean  "selected",       default: true
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
    t.index ["email_stash_id"], name: "index_user_email_stashes_on_email_stash_id"
    t.index ["selected"], name: "index_user_email_stashes_on_selected"
    t.index ["user_id"], name: "index_user_email_stashes_on_user_id"
  end

  create_table "user_mailer_stashes", force: :cascade do |t|
    t.integer  "user_mailer_id"
    t.integer  "email_stash_id"
    t.string   "message_id"
    t.boolean  "is_open",        default: false
    t.string   "email_status"
    t.datetime "created_at",                     null: false
    t.datetime "updated_at",                     null: false
    t.string   "unsubscribe_id"
    t.index ["email_stash_id"], name: "index_user_mailer_stashes_on_email_stash_id"
    t.index ["is_open"], name: "index_user_mailer_stashes_on_is_open"
    t.index ["message_id"], name: "index_user_mailer_stashes_on_message_id"
    t.index ["unsubscribe_id"], name: "index_user_mailer_stashes_on_unsubscribe_id"
    t.index ["user_mailer_id"], name: "index_user_mailer_stashes_on_user_mailer_id"
  end

  create_table "user_mailers", force: :cascade do |t|
    t.integer  "email_template_id"
    t.integer  "user_id"
    t.datetime "start_at"
    t.datetime "end_at"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
    t.string   "subject"
    t.text     "body"
    t.index ["email_template_id"], name: "index_user_mailers_on_email_template_id"
    t.index ["user_id"], name: "index_user_mailers_on_user_id"
  end

  create_table "users", force: :cascade do |t|
    t.string   "name"
    t.string   "token"
    t.string   "uid"
    t.string   "avatar"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.boolean  "admin"
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

end
