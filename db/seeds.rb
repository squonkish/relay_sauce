# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)


EmailStash.create(recipient_email_address: 'andrewrayer@gmail.com', uid: '10109477470370269', recipient_first_name: 'Boris', recipient_last_name: 'Yeltsin', email: 'squonklabs@gmail.com', date_sent: '', selected: true, source: 'yahoo')
EmailStash.create(recipient_email_address: 'relaysauce@gmail.com', uid: '10109477470370269', recipient_first_name: 'Rod',recipient_last_name: 'Cummings', email: 'squonklabs@gmail.com', date_sent: '', selected: true, source: 'yahoo')
EmailStash.create(recipient_email_address: 'scentedpansy@gmail.com', uid: '10109477470370269', recipient_first_name: 'Bootros', recipient_last_name: 'Gali', email: 'squonklabs@gmail.com', date_sent: '', selected: true, source: 'yahoo')
