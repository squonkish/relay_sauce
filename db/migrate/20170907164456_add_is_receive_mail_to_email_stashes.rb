class AddIsReceiveMailToEmailStashes < ActiveRecord::Migration[5.0]
  def change
    add_column :email_stashes, :is_receive_mail, :boolean, default: true
    add_index :email_stashes, :is_receive_mail
  end
end
