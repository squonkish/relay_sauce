class CreateEmailStashes < ActiveRecord::Migration[5.0]
  def change
    create_table :email_stashes do |t|
      t.string :recipient_email_address
      t.string :uid
      t.string :recipient_first_name
      t.string :recipient_last_name
      t.string :email
      t.date :date_sent, null: true
      t.boolean :selected, default: true
      t.string :source

      t.timestamps
    end
  end
end
