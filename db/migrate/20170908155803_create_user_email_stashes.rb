class CreateUserEmailStashes < ActiveRecord::Migration[5.0]
  def change
    create_table :user_email_stashes do |t|
      t.integer :user_id
      t.integer :email_stash_id
      t.boolean :selected, default: true

      t.timestamps
    end

    add_index :user_email_stashes, :user_id
    add_index :user_email_stashes, :email_stash_id
    add_index :user_email_stashes, :selected
  end
end
