class AddUnsubscribeIdToUserMailerStashes < ActiveRecord::Migration[5.0]
  def change
    add_column :user_mailer_stashes, :unsubscribe_id, :string
    add_index :user_mailer_stashes, :unsubscribe_id
  end
end
