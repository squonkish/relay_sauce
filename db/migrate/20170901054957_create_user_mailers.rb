class CreateUserMailers < ActiveRecord::Migration[5.0]
  def change
    create_table :user_mailers do |t|
      t.integer :email_template_id
      t.integer :user_id
      t.datetime :start_at
      t.datetime :end_at

      t.timestamps
    end

    add_index :user_mailers, :user_id
    add_index :user_mailers, :email_template_id
  end
end
