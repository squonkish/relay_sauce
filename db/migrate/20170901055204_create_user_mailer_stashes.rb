class CreateUserMailerStashes < ActiveRecord::Migration[5.0]
  def change
    create_table :user_mailer_stashes do |t|
      t.integer :user_mailer_id
      t.integer :email_stash_id
      t.string :message_id
      t.boolean :is_open, default: false
      t.string :email_status

      t.timestamps
    end

    add_index :user_mailer_stashes, :user_mailer_id
    add_index :user_mailer_stashes, :email_stash_id
    add_index :user_mailer_stashes, :message_id
    add_index :user_mailer_stashes, :is_open
  end
end
