class AddSomeFieldToUserMailer < ActiveRecord::Migration[5.0]
  def change
    add_column :user_mailers, :subject, :string
    add_column :user_mailers, :body, :text
  end
end
