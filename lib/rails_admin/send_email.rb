module RailsAdmin
  module Config
    module Actions
      class SendEmail < RailsAdmin::Config::Actions::Base
        # This ensures the action only shows up for UserMailer
        register_instance_option :visible? do
          authorized? && bindings[:object].class == UserMailer &&
            bindings[:object].start_at.blank?
        end
        register_instance_option :member do
          true
        end
        register_instance_option :link_icon do
          'icon-share-alt'
        end
        register_instance_option :pjax? do
          false
        end
        register_instance_option :controller do
          Proc.new do
            CampaignEmailService.new(@object.id).send_mail
            redirect_to back_or_index
          end
        end
      end
    end
  end
end
