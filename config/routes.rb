require "sidekiq/web"
require 'sidekiq-scheduler/web'

Rails.application.routes.draw do
  devise_for :admins
  authenticate :admin do
    mount RailsAdmin::Engine => '/admin', as: 'rails_admin'
    mount Sidekiq::Web, at: "/relay_sauce_sidekiq"
  end
  
  resources :email_stashes do
    collection do
      put :selection
    end
  end
  #email_stashes
  get 'index_yahoo', to: 'email_stashes#index_yahoo', as: 'email_stashes/index_yahoo'
  get 'index_gmail', to: 'email_stashes#index_gmail', as: 'email_stashes/index_gmail'
  post 'create_recipient', to: 'email_stashes#create_recipient'

  resources :lists


  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root to: "home#index"

  devise_for :users, controllers: { omniauth_callbacks: 'auth/callbacks' }

  devise_scope :user do
    delete 'sign_out', to: 'devise/sessions#destroy', as: :destroy_user_session
  end

  resources :charges

  get '/users/auth/google_oauth2/callback' => 'google_contacts#new'
  get '/users/auth/yahoo_oauth2/callback'  => 'yahoo_contacts#new'

  resources :email_templates

  get 'pricing', to: 'layouts#pricing', as: 'pricing'
  get 'privacy', to: 'layouts#privacy', as: 'privacy'
  get 'about', to: 'layouts#about', as: 'about'
  get 'yahoo_instruction', to: 'layouts#yahoo_instruction', as: 'yahoo_instruction'

  get :send_test_email, to: 'email_templates#send_test_email', as: :send_test_email

  match '/contacts',     to: 'contacts#new',             via: 'get'
  resources "contacts", only: [:new, :create]
  resources 'unsubscribes', only: :show

end
