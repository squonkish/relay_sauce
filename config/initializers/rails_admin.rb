require Rails.root.join('lib', 'rails_admin', 'send_email.rb')
RailsAdmin::Config::Actions.register(RailsAdmin::Config::Actions::SendEmail)

RailsAdmin.config do |config|
  config.authorize_with do |controller|
    unless current_admin
      flash[:error] = "You are not an admin"
      redirect_to main_app.root_path
    end
  end

  config.actions do
    dashboard
    index
    new
    show
    edit
    delete
 
    send_email do
      only UserMailer
    end
  end
end