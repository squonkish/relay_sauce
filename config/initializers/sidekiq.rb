require 'sidekiq'
require 'sidekiq/scheduler'
require "resolv-replace"
REDIS_SERVER_CONFIG = Rails.application.config
  .database_configuration[Rails.env]["redis"]

Sidekiq.configure_server do |config|
  config.on(:startup) do
    Sidekiq.schedule = YAML.load_file(Rails.root.join("config", "sidekiq_schedule.yml"))
    Sidekiq::Scheduler.reload_schedule!
    if Rails.env.production?
      config.redis = {url: REDIS_SERVER_CONFIG['url']}
    else
      host = REDIS_SERVER_CONFIG["host"]
      port = REDIS_SERVER_CONFIG["port"]
      config.redis = {url: "redis://#{host}:#{port}/12"}
    end
    config.failures_max_count = false
    config.failures_default_mode = :all
  end
end

Sidekiq.configure_client do |config|
  if Rails.env.production?
    config.redis = {url: REDIS_SERVER_CONFIG['url']}
  else
    host = REDIS_SERVER_CONFIG["host"]
    port = REDIS_SERVER_CONFIG["port"]
    config.redis = {url: "redis://#{host}:#{port}/12"}
  end
end
